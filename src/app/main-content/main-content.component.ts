import { Component, OnInit } from "@angular/core";
import { Observable, Subject, Subscription, BehaviorSubject } from "rxjs";
// import { PlayVideoComponent } from "../play-video/play-video.component";
import { VideosService } from "../videos.service";
import { Router, ActivatedRoute } from "../../../node_modules/@angular/router";
import { LoginService } from "../login.service";

@Component({
  selector: "app-main-content",
  templateUrl: "./main-content.component.html",
  styleUrls: ["./main-content.component.css"]
})
export class MainContentComponent implements OnInit {
  username;
  videos;
  constructor(
    private vid: VideosService,
    private router: Router,
    private loginServ: LoginService
  ) {}

  ngOnInit() {
    this.videos = this.vid.VIDEOS;
    this.username = this.vid.username;
  }

  loadVid(videoSrc: string) {
    this.vid.vodSubject.next(videoSrc);
  }

  logout() {
    this.loginServ.token = "";
    this.vid.username = "";
    this.username = "";
    // this.router.navigate(["/login"]);
  }

  login() {
    this.router.navigate(["/login"]);
  }
}
