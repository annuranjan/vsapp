import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { LoginService } from "../login.service";
import { VideosService } from "../videos.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  invalidCredentials: boolean = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private logServ: LoginService,
    private vid: VideosService
  ) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  goHomeDirectly() {
    console.log("trying to go home directly");
    this.router.navigate(["home"]);
  }

  onSubmit() {
    this.logServ.login(this.loginForm.value).subscribe(
      response => {
        console.log(response);
        this.logServ.token = response.token;
        this.logServ.username = response.username;
        this.vid.username = response.username;
        this.router.navigate(["/home"]);
      },
      error => {
        if (error.error.message === "Wrong username/password")
          this.invalidCredentials = true;
      }
    );
  }
}
