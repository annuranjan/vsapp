import { Injectable } from "@angular/core";
import { CanActivateChild } from "@angular/router";
import { LoginService } from "./login.service";

@Injectable()
export class PlayVidAuthGuardService implements CanActivateChild {
  constructor(private logServ: LoginService) {}
  canActivateChild() {
    if (this.logServ.token) {
      return true;
    }
    return false;
  }
}
