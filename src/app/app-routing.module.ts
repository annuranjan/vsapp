import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { MainContentComponent } from "./main-content/main-content.component";
import { PlayVideoComponent } from "./play-video/play-video.component";
import { PlayVidAuthGuardService } from "./play-vid-auth-guard.service";
import { LoginComponent } from "./login/login.component";

const appRoutes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  {
    path: "home",
    component: MainContentComponent,
    canActivateChild: [PlayVidAuthGuardService],
    children: [
      {
        path: "",
        component: PlayVideoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
