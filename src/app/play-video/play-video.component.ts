import { Component, OnInit, ViewChild, Input, OnDestroy } from "@angular/core";
import { VideosService } from "../videos.service";
import { Subscription } from "../../../node_modules/rxjs";

@Component({
  selector: "app-play-video",
  templateUrl: "./play-video.component.html",
  styleUrls: ["./play-video.component.css"]
})
export class PlayVideoComponent implements OnInit, OnDestroy {
  @ViewChild("videoPlayer")
  videoplayer: any;

  src;
  srcSubscription = new Subscription();

  constructor(private vid: VideosService) {}

  ngOnInit() {
    this.srcSubscription = this.vid.vodSubject$.subscribe(
      src => (this.src = src)
    );
  }

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }

  ngOnDestroy() {
    this.srcSubscription.unsubscribe();
  }
}
