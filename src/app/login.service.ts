import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { VideosService } from "./videos.service";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  token;
  username;
  urlbase = "http://localhost:1101/";
  url = this.urlbase + "login/";
  constructor(private http: HttpClient) {}

  login(loginBody: any): Observable<any> {
    return this.http.post(this.url, loginBody);
  }
}
