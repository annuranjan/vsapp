import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MatTabsModule } from "@angular/material/tabs";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import {
  MatButtonModule,
  MatButton,
  MatButtonBase
} from "@angular/material/button";
import { MatSelectModule } from "@angular/material/select";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatCardModule, MatCardTitle } from "@angular/material/card";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatListModule } from "@angular/material/list";
import { MatSidenavModule } from "@angular/material/sidenav";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { MainContentComponent } from "./main-content/main-content.component";
import { PlayVideoComponent } from "./play-video/play-video.component";
import { VideosService } from "./videos.service";
import { LoginService } from "./login.service";
import { PlayVidAuthGuardService } from "./play-vid-auth-guard.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainContentComponent,
    PlayVideoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatSidenavModule,

    AppRoutingModule
  ],
  providers: [VideosService, LoginService, PlayVidAuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule {}
