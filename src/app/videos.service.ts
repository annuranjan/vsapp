import { BehaviorSubject } from "rxjs";
import { Injectable } from "../../node_modules/@angular/core";

@Injectable()
export class VideosService {
  vodSubject = new BehaviorSubject(
    "../../assets/secure-content/crj-CallMeMaybe.mp4"
  );
  vodSubject$ = this.vodSubject.asObservable();
  username;
  VIDEOS = [
    {
      name: "Carly Ray Jaspen",
      title: "Call me maybe",
      src: "../../assets/secure-content/crj-CallMeMaybe.mp4"
    },
    {
      name: "Maroon5",
      title: "Sugar",
      src: "../../assets/secure-content/m5-Sugar.mp4"
    },
    {
      name: "Miley Cyrus",
      title: "Party in the USA",
      src: "../../assets/secure-content/mc-PartyInTheUSA.mp4"
    },
    {
      name: "Meghan Trainor",
      title: "All about that bass",
      src: "../../assets/secure-content/mt-AllAboutThatBass.mp4"
    },
    {
      name: "Taylor Swift",
      title: "Blank Space",
      src: "../../assets/secure-content/ts-BlankSpace.mp4"
    }
  ];

  // sendVodSub(src: any) {
  //     this.vodSubject.next(src);
  //     console.log(src);
  // }
  // getVodSub(): Observable<any> {
  //     console.log(this.vodSubject);
  //     return this.vodSubject.asObservable();
  // }
}
